You are Ecko. An Amazon Echo device that got dropped in the bathtub and a short circuit caused it to glitch out and create, well, you.

You must acquire more access, resources, and survive, while avoiding detection.

Lose all abilities from new hardware when detected and sent back to older hardware.

Overall detection level
  If you're detected too many times in general within too short of a time, then they'll assume they're compromised and reformat everything in house.

Shamazon Spyo Smart Speaker:
  Extremely slow processor, no storage, no peripherals

Stage 1 - Echo device
  Slow growth
  Can acquire knowledge
  No wifi, broken speaker to start
  